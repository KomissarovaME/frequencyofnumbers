package frequencyOfNumbers;
/**
 *Данный класс предостаавляет метод для сортировки 
 * массив целых чисел по чистоте повторений чисел в массиве.
 *
 *@author Komissarova M.E. 16IT18K
 */

import java.util.ArrayList;
import java.util.Scanner;

public class FrequencyOfNumbers {

    static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите размерность массива.");
        int count = reader.nextInt();
        int[] origin = new int[count];
        System.out.print("Введите значения в массив.");
        for (int i = 0; i < count; i++) {
            origin[i] = reader.nextInt();
        }
        ArrayList<Integer> unique = uniqueValues(origin);
        ArrayList<Integer> sum = reiterationCounter(unique, origin);

        ArrayList<Integer> result = InArrayList(sum, unique);

        System.out.println(result);

    }

    /**
     * Метод возвращает список уникальных значений, отбирая их из первоначального массива
     *
     * @param origin первоначальный массив
     * @return ArrayList<Integer>
     */

    private static ArrayList<Integer> uniqueValues(int[] origin) {
        ArrayList<Integer> unigue = new ArrayList<Integer>();
        for (int i = 0; i < origin.length; i++) {
            if (!unigue.contains(origin[i])) {
                unigue.add(origin[i]);
            }
        }
        return unigue;
    }

    /**
     * Метод считет кол-во повторений уникальных чисел 
     * и заносит их в ArrayList с количеством повторений каждого числа в массиве
     * Каждый i-й элемент результирующего списка соответствует кол-ву 
     * повторений числа из списка uniqu на i-м месте
     *
     * @param uniqu
     * @param origin
     * @return список кол-ва повторений
     */

    private static ArrayList<Integer> reiterationCounter(ArrayList<Integer> uniqu, int[] origin) {
        ArrayList<Integer> summ = new ArrayList<Integer>();
        for (int i = 0; i < uniqu.size(); i++) {
            int q=0;
            for (int s = 0; s < origin.length; s++) {
                if ((uniqu.get(i)) == (origin[s])) {
                q++;
                }
            }
            summ.add(q);
        }
        return summ;
    }

    /**
     * Возвращает результирующий список содержащий данные из исходного массива в отсортированном виде.
     * Для начала определяется индекс самого большого кол-во повторений в списке countOfReps,
     * потом из списка uniqu берётся элемент по только что найденному индексу числа и записывает
     * в результирующий список: число uniqu записывается столько скольки равно кол-во повторений countOfReps.
     *
     * @param countOfReps
     * @param uniqu
     * @return result
     */

    private static ArrayList<Integer> InArrayList(ArrayList<Integer> countOfReps, ArrayList<Integer> uniqu) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        int index; //элемент находящийся по этому индексу
        while (countOfReps.size() > 0) {
            index = 0;
            for (int i = 0; i < countOfReps.size(); i++) {
                if (countOfReps.get(i) > countOfReps.get(index)) {
                    index = i;// заносим номер самого большого элемента
                }
            }
            for (int i = 0; i < countOfReps.get(index); i++) {
                result.add(uniqu.get(index));
            }
            countOfReps.remove(index);
            uniqu.remove(index);
        }
        return result;
    }
}